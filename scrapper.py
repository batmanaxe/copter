#script retournant sous forme de fichiers excel distincts la liste des 
# des constructeurs et modèles d'hélicoptères militaires

import requests
from bs4 import BeautifulSoup as bs
import pandas as pd
import numpy as np
import urllib.requestscript

#création de la fonction infobox permetant de récupérer les données en provenance de l'infobox d'une page Wiki

def infobox(query):    
    url = 'https://fr.wikipedia.org/wiki/'+ query
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36', 'Referer': 'https://www.nseindia.com/'}
    r = requests.get(url,  headers=headers)
    soup = bs(r.content,'lxml')
    table =soup.select_one('.infobox_v2')
    if table ==None:
        column_names = ["caracteristiques", query]
        df = pd.DataFrame(columns = column_names)
        
        df = pd.DataFrame(np.NaN, index = [0], columns = ['caracteristiques', query])
        
        return df
    else:
        rows = table.find_all('tr')
        output = []
        for row in rows:
            if len(row.select('th, td')) == 2:
                outputRow = [row.select_one('th').text, row.select_one('td').text]
                output.append(outputRow)
        df = pd.DataFrame(output)
        df.columns=['caracteristiques',query]
        df['caracteristiques']=df['caracteristiques'].str.strip('\n')
        df[query]=df[query].str.strip('\n')
        return df
    

# On parcourt la page Wiki listant les hélicoptères militaires et les constructeurs et on crée une liste indiférenciée des deux items

html_page = urllib.request.urlopen("https://fr.wikipedia.org/wiki/Liste_d%27h%C3%A9licopt%C3%A8res_civils_et_militaires#H%C3%A9licopt%C3%A8res_militaires")
soup = bs(html_page, "html.parser")
link_list=[]
for link in soup.findAll('a'):
    link_list.append(link.get('href'))
    
new_list=[]
for elem in link_list:
    if elem!=None and elem.startswith('/wiki/'):
        a = elem.replace('/wiki/','') 
        new_list.append(a)

new_list = sorted(set(new_list))


for i in range(1,10):
    for elem in new_list:
        if elem.startswith('Aide') or elem.startswith('Cat') or elem.startswith('Fichier:') or elem.startswith('Liste') or elem.startswith('Sp%C3%A9cial')or elem.startswith('Wikip') or elem.startswith('Portail'):
            new_list.remove(elem)
        
len(new_list)

from urllib.parse import unquote
listus=[]
for elem in new_list:
    a=unquote(elem)
    listus.append(a)

count=0
list_constructeur=[]
list_helicoptere=[]
list_crap=[]
for elem in listus[0:287]:
    if infobox(elem)['caracteristiques'][0]=='Création':
        list_constructeur.append(infobox(elem))
        print('construct')
        count+=1
    elif infobox(elem)['caracteristiques'][0]=='Rôle':
        list_helicoptere.append(infobox(elem))
        print('heli')
        count+=1
    else:
        list_crap.append(infobox(elem))
        print('crap')
        count+=1

#construction des dataframes helicoptères et constructeurs

from functools import reduce

df_merged_heli = reduce(lambda  left,right: pd.merge(left,right,on=['caracteristiques'],
                                            how='outer'), list_helicoptere)

# if you want to fill the values that don't exist in the lines of merged dataframe simply fill with required strings as
df_merged_constructeur = reduce(lambda  left,right: pd.merge(left,right,on=['caracteristiques'],
                                            how='outer'), list_constructeur)

#définition des fonctions de nettoyage des dataframes

def cleaner_heli(df):
    
    list_to_drop = ['Versions','Équipage', 'Retrait', 'Client principal', 'Production', 'Passagers', 'Moteurs']
    
    df.columns= df.columns.str.replace("_"," ")
    
    df.index=df['caracteristiques']
    df = df.drop(['caracteristiques'], axis=1)
    df = df.transpose()
    
    df = df.rename(columns={"Interne": "Armement Interne", "Externe": "Armement Externe"})
    
    df = df.drop(list_to_drop,axis=1)

    return df

def cleaner_constructeur(df):
    
    #list_to_drop = ['Versions','Équipage', 'Retrait', 'Client principal', 'Production', 'Passagers', 'Moteurs']
    
    df.columns= df.columns.str.replace("_"," ")
    
    df.index=df['caracteristiques']
    df = df.drop(['caracteristiques'], axis=1)
    df = df.transpose()
    
    #df = df.rename(columns={"Interne": "Armement Interne", "Externe": "Armement Externe"})
    
    #df = df.drop(list_to_drop,axis=1)

    return df


    #création des fichiers excel

df_clean_heli = cleaner_heli(df_merged_heli)
df_clean_heli.to_excel('Le grand fichier des helicos.xlsx', sheet_name='Your sheet name')
df_clean_const= cleaner_constructeur(df_merged_constructeur)
df_clean_const.to_excel('Le grand fichier des constructeurs.xlsx', sheet_name='Your sheet name')